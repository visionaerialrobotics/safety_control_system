# Recovery manager

## Subscribed topics

- **emergency_event** ([aerostack_msgs/StringStamped](https://bitbucket.org/visionaerialrobotics/aerostack_msgs/src/5fcf0e3de4e41504bbf610fa5345ee108f6bf19f/msg/StringStamped.msg))  
Emergency event.


----
# Contributors

**Code maintainer:** Javier Cabrera

**Authors:** Javier Cabrera (programming and testing), Martin Molina (specification)
