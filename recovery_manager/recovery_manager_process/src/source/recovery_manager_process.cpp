/*!*******************************************************************************************
 *  \file       recovery_manager_process.cpp
 *  \brief      Recovery manager implementation file.
 *  \details    
 *  \authors    Javier Cabrera Marugan
 *              Martín Molina
 *  \copyright  Copyright (c) 2019 Universidad Politecnica de Madrid
 *              All rights reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/ 

#include "recovery_manager_process.h"
bool token=true;
RecoveryManagerProcess::RecoveryManagerProcess()
{}

RecoveryManagerProcess::~RecoveryManagerProcess()
{}

double RecoveryManagerProcess::get_moduleRate()
{ 
 return rate;
}

void RecoveryManagerProcess::ownSetUp()
{   // Configs
    //
    ros::param::get("~robot_namespace", robot_namespace);
    if ( robot_namespace.length() == 0) robot_namespace = "drone1";
    std::cout << "robot_namespace=" <<robot_namespace<< std::endl;
    ros::param::get("~frequency", rate);
    ros::param::get("~drone_id", id);
}

void RecoveryManagerProcess::ownStart(){    
    std::cout<<"STARTING RECOVERY MANAGER"<<std::endl;
    // Services  
    recovery_plan_server = n.advertiseService("/" + robot_namespace +"/execute_recovery_plan", &RecoveryManagerProcess::executeRecoveryPlan, this);
    // Clients of services
    activate_behavior_srv = n.serviceClient<aerostack_msgs::RequestBehaviorActivation>("/" + robot_namespace +"/request_behavior_activation");

    query_client = n.serviceClient<aerostack_msgs::QueryBelief>("query_belief");
    remove_client = n.serviceClient<aerostack_msgs::RemoveBelief>("remove_belief");

}

void RecoveryManagerProcess::ownStop(){}   

void RecoveryManagerProcess::ownRun(){}


bool RecoveryManagerProcess::return_starting_point(){
  aerostack_msgs::RequestBehaviorActivation::Request msg;
  aerostack_msgs::RequestBehaviorActivation::Response res;
  aerostack_msgs::BehaviorCommandPriority beh;
	beh.name = "WAIT";
	beh.priority = 3;
	beh.arguments = "duration: "+std::to_string(1)+"";
	msg.behavior=beh;
	std::cout<<beh.arguments<<std::endl;
  activate_behavior_srv.call(msg,res);
  std::cout<<"acabo de ejecutarlo"<<std::endl;
  while(true){
  msg_from_activation = *ros::topic::waitForMessage<aerostack_msgs::BehaviorActivationFinished>("/" + robot_namespace + "/behavior_activation_finished", n);
  if(msg_from_activation.name  == "WAIT") break;
  }
  beh.name = "FOLLOW_PATH";
  beh.priority = 3;
  beh.arguments = "path: [[0, 0, 1]]";
  msg.behavior=beh;
  activate_behavior_srv.call(msg,res);
  while(true){
  msg_from_activation = *ros::topic::waitForMessage<aerostack_msgs::BehaviorActivationFinished>("/" + robot_namespace + "/behavior_activation_finished", n);
  if(msg_from_activation.name  == "FOLLOW_PATH") break;
  }
  beh.name = "LAND";
  beh.priority = 3;
  beh.arguments = "";
  msg.behavior=beh;
  activate_behavior_srv.call(msg,res);
  while(true){
  msg_from_activation = *ros::topic::waitForMessage<aerostack_msgs::BehaviorActivationFinished>("/" + robot_namespace + "/behavior_activation_finished", n);
  if(msg_from_activation.name  == "LAND") break;
  }  
  return true;
}
bool RecoveryManagerProcess::wait(){
  aerostack_msgs::RequestBehaviorActivation::Request msg;
  aerostack_msgs::RequestBehaviorActivation::Response res;
  aerostack_msgs::BehaviorCommandPriority beh;
	beh.name = "WAIT";
	beh.priority = 3;
	srand((unsigned) time(0) + id*100);
	int time=rand()%5 +1;
	time=time + rand()%5 +1;
	beh.arguments = "duration: "+std::to_string(time)+"";
	msg.behavior=beh;
  activate_behavior_srv.call(msg,res);
  while(true){
  msg_from_activation = *ros::topic::waitForMessage<aerostack_msgs::BehaviorActivationFinished>("/" + robot_namespace + "/behavior_activation_finished", n);
  if(msg_from_activation.name  == "WAIT") break;
  }
  return true;
}


std::vector<std::string> RecoveryManagerProcess::getpairs(std::string subs){
std::vector<std::string> recortes;
   int ini=0;
   int pos=0;
    while((pos=subs.find("\n",pos))!=std::string::npos){
      recortes.push_back(subs.substr(ini,pos-ini));
      pos=pos+1;
      ini=pos;

  }
//now we are going to delete spaces
  std::vector<std::string> res;
  for(int j=0;j<recortes.size();j++){
      std::string aux="";
      for(int  i = 0; recortes[j][i] != 0;i++){
              if(recortes[j][i] != 32){
                  aux=aux+recortes[j][i];
              }
      }
      res.push_back(aux);
  }

return res;
}

std::vector<std::string> RecoveryManagerProcess::getvars(std::vector<std::string> pairs){

  std::vector<std::string>res;
  for (int i=0; i<pairs.size();i++){
    res.push_back(pairs[i].substr(0,pairs[i].find(":")));
  }

   for (int i=0; i<pairs.size();i++){
       res[i]="?"+res[i];
   }

  return res;
}
std::vector<std::string> RecoveryManagerProcess::getsubs(std::vector<std::string> pairs){

  std::vector<std::string>res;
  for (int i=0; i<pairs.size();i++){
     res.push_back(pairs[i].substr(pairs[i].find(":")+1,pairs[i].size()-1));
  }
  return res;
}




bool RecoveryManagerProcess::ask_for_access(){
  aerostack_msgs::QueryBelief srv;
  srv.request.query = "self(?x)"; 
  query_client.call(srv);
  aerostack_msgs::QueryBelief::Response response= srv.response;
  if(response.success==false){return false;}
  auto sub = response.substitutions;
  std::vector<std::string> pairs = getpairs(sub);
  std::vector<std::string> subs = getsubs(pairs);
  int id_drone1 = std::stoi(subs[0]);

  srv.request.query = "frontal_collision_course("+std::to_string(id_drone1)+",?y)"; 
  query_client.call(srv);
  response= srv.response;
  if(response.success==false){return false;}
  sub = response.substitutions;
  pairs = getpairs(sub);
  subs = getsubs(pairs);
  int id_drone2 = std::stoi(subs[0]);

  srv.request.query = "name("+std::to_string(id_drone1)+",?x)";
  query_client.call(srv);
  response= srv.response;
  if(response.success==false){return false;}
  sub = response.substitutions;
  pairs = getpairs(sub);
  subs = getsubs(pairs);
  std::string name_drone1 = subs[0];

  srv.request.query = "name("+std::to_string(id_drone2)+",?x)";
  query_client.call(srv);
  response= srv.response;
  if(response.success==false){return false;}
  sub = response.substitutions;
  pairs = getpairs(sub);
  subs = getsubs(pairs);
  std::string name_drone2 = subs[0];

	srv.request.query = "position("+std::to_string(id_drone1)+",(?x,?y,?z))"; 
  query_client.call(srv);
  response= srv.response;
  if(response.success==false){return false;}
  sub = response.substitutions;
  pairs = getpairs(sub);
	subs = getsubs(pairs);
	std::vector<std::string> vars = getvars(pairs);
	double x=0;
	double y=0;
	double z=0;
	for(int i=0;i<subs.size();i++){
		if(vars[i]=="?x"){
			x=std::stod(subs[i]);
		}else if (vars[i]=="?y"){
			y=std::stod(subs[i]);
		}else{
			z=std::stod(subs[i]);
		}
	}


  if (name_drone1<=name_drone2){
    std::cout<<"I am first"<< std::endl;
    while(true){
    srv.request.query = "object(?x,message), sender(?x, "+std::to_string(id_drone2)+"), text(?x, you_can_pass)"; 
	  query_client.call(srv);
	  response= srv.response;
	  if(response.success==true){
      auto sub = response.substitutions;
      std::vector<std::string> pairs = getpairs(sub);
      std::vector<std::string> subs = getsubs(pairs);
      aerostack_msgs::RemoveBelief::Request req;
      aerostack_msgs::RemoveBelief::Response res;
      std::stringstream s;
      s.str(std::string());
      s << "object("<<subs[0]<<",message), sender("<<subs[0]<<", "<<std::to_string(id_drone2)<<"), text("<<subs[0]<<", you_can_pass)";
      req.belief_expression = s.str();
      remove_client.call(req, res);
      break;
    } 
	  aerostack_msgs::RequestBehaviorActivation::Request msg;
		aerostack_msgs::RequestBehaviorActivation::Response res;
	  aerostack_msgs::BehaviorCommandPriority beh;
    beh.name = "WAIT";
		beh.priority = 3;
		int time=1;
		beh.arguments = "duration: "+std::to_string(time)+"";
		msg.behavior=beh;
	  activate_behavior_srv.call(msg,res);
		while(true){
		  msg_from_activation = *ros::topic::waitForMessage<aerostack_msgs::BehaviorActivationFinished>("/" + robot_namespace + "/behavior_activation_finished", n);
		  if(msg_from_activation.name  == "WAIT") break;
		}

	  }


    }else{
    	std::cout<<"I am second"<< std::endl;
    	aerostack_msgs::RequestBehaviorActivation::Request msg;
		  aerostack_msgs::RequestBehaviorActivation::Response res;
	    aerostack_msgs::BehaviorCommandPriority beh;
      beh.name = "WAIT";
		  beh.priority = 3;
		  int time=1;
		  beh.arguments = "'duration': "+std::to_string(time)+"";
		  msg.behavior=beh;
	  	activate_behavior_srv.call(msg,res);
		  while(true){
		    msg_from_activation = *ros::topic::waitForMessage<aerostack_msgs::BehaviorActivationFinished>("/" + robot_namespace + "/behavior_activation_finished", n);
		    if(msg_from_activation.name  == "WAIT") break;
		  }

	    z=z+2;
		  beh.name = "FOLLOW_PATH";
	    beh.priority = 3;
	    beh.arguments = "'path': [["+std::to_string(x)+", "+std::to_string(y)+", "+std::to_string(z)+"]]";
	    msg.behavior=beh;
	    activate_behavior_srv.call(msg,res);
	    while(true){
	    msg_from_activation = *ros::topic::waitForMessage<aerostack_msgs::BehaviorActivationFinished>("/" + robot_namespace + "/behavior_activation_finished", n);
	    if(msg_from_activation.name  == "FOLLOW_PATH") break;
	    }
	    
	    beh.name = "INFORM_ROBOTS";
		  beh.priority = 3;
		  beh.arguments = "{'text': 'you_can_pass', 'destination':'"+name_drone2+"'}";
		  msg.behavior=beh;
		  activate_behavior_srv.call(msg,res);
	    beh.name = "WAIT";
		  beh.priority = 3;
	  	time=8;
	  	beh.arguments = "'duration': "+std::to_string(time)+"";
	  	msg.behavior=beh;
  		activate_behavior_srv.call(msg,res);
	  	while(true){
		    msg_from_activation = *ros::topic::waitForMessage<aerostack_msgs::BehaviorActivationFinished>("/" + robot_namespace + "/behavior_activation_finished", n);
		    if(msg_from_activation.name  == "WAIT") break;
		  }
      z=z-2;
	  	beh.name = "FOLLOW_PATH";
	    beh.priority = 3;
	    beh.arguments = "'path': [["+std::to_string(x)+", "+std::to_string(y)+", "+std::to_string(z)+"]]";
	    msg.behavior=beh;
	    activate_behavior_srv.call(msg,res);
	    while(true){
	    msg_from_activation = *ros::topic::waitForMessage<aerostack_msgs::BehaviorActivationFinished>("/" + robot_namespace + "/behavior_activation_finished", n);
	    if(msg_from_activation.name  == "FOLLOW_PATH") break;
	    }
    }
    return true;
}

bool RecoveryManagerProcess::executeRecoveryPlan(aerostack_msgs::RecoveryPlan::Request& req,
                                        aerostack_msgs::RecoveryPlan::Response& res) {

	if(req.plan == "return_starting_point"){
            res.ack=return_starting_point();
	}else if(req.plan == "wait"){
		    res.ack=wait();    
	}else if(req.plan == "ask_for_access"){
            res.ack=ask_for_access();
	}
	else{
		std::cout<<"[RECOVERY MANAGER] Mission not found"<<std::endl;exit(-1);
	}
	return true;
}


        