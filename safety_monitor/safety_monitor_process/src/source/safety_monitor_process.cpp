/*!*******************************************************************************************
 *  \file       emergency_manager_process.cpp
 *  \brief      Emergency manager implementation file.
 *  \details    
 *  \authors    Javier Cabrera Marugan
 *              Martín Molina
 *  \copyright  Copyright (c) 2019 Universidad Politecnica de Madrid
 *              All rights reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/ 

#include "safety_monitor_process.h"

SafetyMonitorProcess::SafetyMonitorProcess()
{}

SafetyMonitorProcess::~SafetyMonitorProcess()
{}

double SafetyMonitorProcess::get_moduleRate()
{ 
 return rate;
}

void SafetyMonitorProcess::ownSetUp()
{   // Configs
    //
    ros::param::get("~robot_namespace", robot_namespace);
    if ( robot_namespace.length() == 0) robot_namespace = "drone1";
    std::cout << "robot_namespace=" <<robot_namespace<< std::endl;
    ros::param::get("~safety_monitor_language_path",safety_monitor_language_path);
    ros::param::get("~safety_monitor_language_file",safety_monitor_language_file);
    ros::param::get("~frequency", rate);
}

void SafetyMonitorProcess::ownStart(){    
    std::cout<<"STARTING SAFETY MONITOR"<<std::endl;
    

    //Subscribers
    emergency_event_subscriber = n.subscribe("/"+robot_namespace+"/emergency_event", 1, &SafetyMonitorProcess::emergencyEventCallback, this);

    //Services
    activate_behavior_srv = n.serviceClient<aerostack_msgs::RequestBehaviorActivation>("/" + robot_namespace +"/request_behavior_activation");
    deactivate_behavior_srv = n.serviceClient<aerostack_msgs::RequestBehaviorDeactivation>("/" + robot_namespace +"/request_behavior_deactivation");
    plan_srv = n.serviceClient<aerostack_msgs::RecoveryPlan>("/" + robot_namespace +"/execute_recovery_plan");
    stop_mission = n.serviceClient<std_srvs::Empty>("/"+robot_namespace+"/python_based_mission_interpreter_process/stop");


    try{
      safety_rules = YAML::LoadFile(safety_monitor_language_path+"/"+safety_monitor_language_file);
    }catch(std::exception &e){
    std::cout<<"Failed to load yaml file"<<safety_monitor_language_file;
    return;
  }
  last_alternative_behavior=std::make_pair(std::string(""),0);

}

void SafetyMonitorProcess::ownStop(){}   

void SafetyMonitorProcess::ownRun(){}

//Callbacks
void SafetyMonitorProcess::emergencyEventCallback(const aerostack_msgs::StringStamped& msg){
    std::cout<<"emergencia encontrada"<<std::endl;
    //Get msg
    std::string belief_predicate = ""; 
    std::string belief_value = ""; 
    bool pred_bool = true;
    bool pred_value = false;
    for (auto x : msg.data){ 
        if (x == '(') pred_bool = false;
        if (x == ')') pred_value = false;
        if (pred_bool) belief_predicate = belief_predicate + x; 
        if (pred_value && x != ' ') belief_value = belief_value + x; 
        if (x == ',') pred_value = true;   
    }
    std::cout<<belief_predicate<<std::endl;
    std::cout<<belief_value<<std::endl;
    YAML::Node recovery_actions;
    YAML::Node activations;
    YAML::Node parameters;
    YAML::Node item_parameters;
    std::map<std::string,std::string> map_parameters;
    if(!(recovery_actions = safety_rules["recovery_actions_for_emergency_events"])){
        std::cout<<"error, no recovery_actions_for_emergency_events";
    }

    for (std::size_t i=0;i<recovery_actions.size();i++){
        YAML::Node recovery_action = recovery_actions[i];
        if((recovery_action["predicate"].as<std::string>()==belief_predicate) && (!recovery_action["value"] || recovery_action["value"].as<std::string>()==belief_value)) {

          if(recovery_action["abort_mission"].as<std::string>() == "TRUE"){
            std::cout<<"estoy abortando"<<std::endl;
    			 //Stop mission
    			  std_srvs::Empty stop_mission_msg;
    			  stop_mission.call(stop_mission_msg);
          }
          if(recovery_action["recovery_plan"]){
            aerostack_msgs::RecoveryPlan::Request msg_rec;
            aerostack_msgs::RecoveryPlan::Response res_rec;
            std::string plan= recovery_action["recovery_plan"].as<std::string>();
            msg_rec.plan = plan;
            plan_srv.call(msg_rec,res_rec);
            if(!res_rec.ack) {
                   std::cout<<"[SAFETY MONITOR] Recovery plan not found"<<std::endl;
                return;
            }

          }
          if((activations=recovery_action["behavior_activations"])){
              for (std::size_t j=0;j<activations.size();j++){
                YAML::Node behavior = activations[j];
                std::string name= behavior["behavior"].as<std::string>();

				        aerostack_msgs::RequestBehaviorActivation::Request msg;
          		  aerostack_msgs::RequestBehaviorActivation::Response res;
          		  aerostack_msgs::BehaviorCommandPriority behavior_to_send;
          		  behavior_to_send.name = name;
          		  behavior_to_send.priority = 3;

                if(parameters = behavior["parameters"]){
                	for(std::size_t k=0;k<parameters.size();k++){
                		YAML::Node item_parameters =parameters[k];
                		map_parameters[item_parameters["parameter_name"].as<std::string>()] = item_parameters["parameter_value"].as<std::string>();
                	}
          			for (std::map<std::string,std::string>::iterator it=map_parameters.begin(); it!=map_parameters.end(); ++it){
          				int value;
          				if(value = stof(it->second)){
          					behavior_to_send.arguments.append(it->first + ": " + std::to_string(value) + "\n");
          				}
          				else{
          					behavior_to_send.arguments.append( it->first + ": \"" + it->second+ "\"" + "\n");
          				}
          			}
          		}
              std::cout<<behavior_to_send.name<<std::endl;
              std::cout<<behavior_to_send.arguments<<std::endl;
          	  msg.behavior = behavior_to_send;
          		activate_behavior_srv.call(msg,res);
          		if(!res.ack) {
                   std::cout << res.error_message << std::endl;
                return ;
              }
          	}
          }
        }
    }
}
        